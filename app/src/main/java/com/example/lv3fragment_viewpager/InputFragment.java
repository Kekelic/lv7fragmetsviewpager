package com.example.lv3fragment_viewpager;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;


public class InputFragment extends Fragment {

    private EditText mEditText;
    private Button mSubmitButton;
    private TextView tvMessage;


    public static InputFragment newInstance() {
        InputFragment fragment= new InputFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_input, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mEditText = view.findViewById(R.id.edMessage);
        mSubmitButton = view.findViewById(R.id.btSubmit);
        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvMessage = (TextView) getActivity().findViewById(R.id.tvMessage);
                if(tvMessage!=null)
                    tvMessage.setText(mEditText.getText().toString());
                ViewPager mViewPager;
                mViewPager = getActivity().findViewById(R.id.viewPager);
                mViewPager.setCurrentItem(1);
            }
        });
    }

}
